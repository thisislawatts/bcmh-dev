<?php 

global $wpsf_settings;

$wpsf_settings[] = array(
	'section_id' => 'vcard',
	'section_title' => 'vCard Information',
	'section_description' => 'Enter the company information, to allow for the output of structured data elsewhere in the theme.',
	'section_order' => 6,
	'fields' => array(
		array(
			'id' 	=> 'organisation_name',
			'title' => 'Organisation Name',
			'type'	 => 'text',
			'std' 	=> '',
		),
		array(
			'id' 	=> 'street_address',
			'title' => 'Street Address',
			'type' 	=> 'textarea',
			'std'	=> '',
		),
		array(
			'id'	=> 'address_locality',
			'title' => 'Locality',
			'type'	=> 'text',
			'std'	=> '',
		),
		array(
			'id'	=> 'address_country',
			'title' => 'Country',
			'type'	=> 'text',
			'std'	=> '',
		),
		array(
			'id'	=> 'address_postalcode',
			'title'	=> 'Postal Code',
			'type'  => 'text',
			'std'	=> '',
		),
		array(
			'id'	=> 'fax',
			'title' => 'Fax',
			'type' 	=> 'tel',
			'std'	=> '',
		),
		array(
			'id'	=> 'telephone',
			'title' => 'Telephone',
			'type' 	=> 'tel',
			'std'	=> '',
		),
		array(
			'id'	=> 'email',
			'title' => 'Email',
			'type' 	=> 'email',
			'std'	=> '',
		),
		array(
			'id'	=> 'lat',
			'title'	=> 'Latitude',
			'type'	=> 'text',
			'std'	=> ''
		),
		array(
			'id'	=> 'long',
			'title'	=> 'Longitude',
			'type'	=> 'text',
			'std'	=> ''
		)
	),
);
