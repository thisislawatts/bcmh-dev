<?php
/*
Plugin Name: BCMH Dev Tools
Plugin URI: http://www.bcmh.co.uk
Description: A plugin for helping development within BCMH
Author: Luke Watts
Version: 0.9
Author URI: http://github.com/thisislawatts
*/


// Derive the current path and load up Sanity
$plugin_path = dirname(__FILE__).'/';

if ( class_exists('SanityPluginFramework') != true ) {
	require_once( $plugin_path . 'framework/sanity.php' );
}

if ( !class_exists('BCMHWidget') )
	require_once $plugin_path . 'framework/widget.php';

require_once( $plugin_path . 'framework/wp-settings-framework.php' );

/*
*		Define your plugin class which extends the SanityPluginFramework
*		Make sure you skip down to the end of this file, as there are a few
*		lines of code that are very important.
*/ 
class BCMH_Dev extends SanityPluginFramework {
	
	/*
	*	Some required plugin information
	*/
	var $version = '1.0';
	var $plugin_url;
	var $plugin_path;
	var $wpsf;

	/*
	*		Required __construct() function that initalizes the Sanity Framework
	*/
	function __construct() {
      parent::__construct(__FILE__);

      global $plugin_path;

      $this->plugin_url = plugin_dir_url("") . basename(dirname(__FILE__)) . "/";

      $this->plugin_path = $plugin_path;

      $this->wpsf = new WordPressSettingsFramework( $plugin_path . 'settings/settings-general.php' );
      add_filter( $this->wpsf->get_option_group() . '_settings_validate', array( &$this, 'validate_settings' ) );

      add_action('admin_menu', array(&$this, 'admin_menu'));
      add_action('init', array(&$this, 'hide_site'));
	  add_action( 'widgets_init', array( &$this, 'register_widgets' ) );

  	}

	/*
	*		Run during the activation of the plugin
	*/
	function activate() {
	}
	
	

	/*
	*		Run during the initialization of Wordpress
	*/
	function initialize() {
		// Setup BCMH login alterations
		add_filter('login_headerurl', array(&$this, 'login_url'));
		add_action('login_head', array(&$this, 'login_styles'));
		// Admin alterations
		add_filter('admin_footer_text', array(&$this, 'admin_footer'));

		// BCMH vCards
		add_shortcode('BCMHvcard', array( &$this, 'vcard' ) );
	}

	function register_widgets() {
		register_widget( 'BCMHWidget' );
	}
	
	/* 		Options */

	function admin_init() {

		global $current_user;

		$role = get_role( 'editor' );

		$role->remove_cap( 'editor', 'moderate_comments');
		$role->remove_cap( 'editor', 'edit_comment');

		// Remove items for non Admin users
		$current_user_role = array_shift( wp_get_current_user()->roles );

		if ( $current_user_role != "administrator" )  {
			remove_menu_page('edit-comments.php');
		}
	}

	function admin_menu() {
		global $current_user;

		add_options_page('BCMH Development Tools', 'BCMH', 'manage_options', 'bcmh_options', array(&$this, 'options_page'));
	}

	function validate_settings( $input ) {

		return $input;
	}

	/* Renders our option page */
	function options_page() {
		$this->data = array();
			
		echo $this->render('admin');
	}

	/* 		Hide Site */
	function hide_site() {

		$login = in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'));

 		if (preg_match('/login/i', $_SERVER['REQUEST_URI']) && !$login) {
 			header("Location: " . get_bloginfo('wpurl') ."/wp-login.php" );
 			die();
 		}


		if ( !is_user_logged_in() && !$login && !preg_match( '/\.loc/', get_bloginfo('wpurl') ) ) {

 			wp_enqueue_script('keymaster', $this->plugin_url . 'js/keymaster.min.js', null, false, true);
 			wp_enqueue_script('login-js', $this->plugin_url . 'js/login.js', array('keymaster'), false, true);
	 		echo $this->render('static/hold');
	 		die();			
		}
	}

	/* 		Login Page */
	function login_url() {
		return "http://bcmh.co.uk/";
	}
	function login_styles() {
		wp_enqueue_style( 'login_css', $this->plugin_url . "css/login.css" );
	}

	/* 		Admin Page */
	function admin_footer() {
		echo '&copy; ' . date("Y") . ' <a href="http://bcmh.co.uk">BCMH</a>';
	}

	/* 		Vcard Handler */
	function vcard( $atts ) {

		extract( shortcode_atts( array(
			'class' => 'standard'
		) , $atts ) );

		$settings = wpsf_get_settings( $this->plugin_path . 'settings/settings-general.php' ); ?>

		<div itemscope itemtype="http://schema.org/LocalBusiness" class="vcard <?php echo $class; ?>">
		<span itemprop="name" class="org"><?php echo $settings['settingsgeneral_vcard_organisation_name'] ?></span>
		<div class="adr" itemscope itemtype="http://schema.org/PostalAddress">
		<span class="street-address" itemprop="streetAddresss"><?php echo $this->tag_string( $settings['settingsgeneral_vcard_street_address']); ?></span>
		<span class="locality" itemprop="addressLocality"><?php echo $settings['settingsgeneral_vcard_address_locality']; ?></span>
		<span class="country-name" itemprop="addressCountry"><?php echo $settings['settingsgeneral_vcard_address_country']; ?></span>
		<span class="postal-code" itemprop="postalCode"><?php echo $settings['settingsgeneral_vcard_address_postalcode']; ?></span>
		</div><!-- .adr -->
		
		<a href="tel:<?php echo $this->normalize_tel_no( $settings['settingsgeneral_vcard_telephone'] ); ?>"><span itemprop="telephone" class="tel"><?php echo $settings['settingsgeneral_vcard_telephone']; ?></span></a>
		<?php if ( $settings['settingsgeneral_vcard_fax'] != "" ) : ?>
			<span class="fax" itemprop="fax"><?php echo $settings['settingsgeneral_vcard_fax']; ?></span>
		<?php endif; ?>
		<a href="mailto:<?php echo $settings['settingsgeneral_vcard_email']; ?>"><span itemprop="email" class="email"><?php echo $settings['settingsgeneral_vcard_email']; ?></span></a>
			<?php if ( $settings['settingsgeneral_vcard_lat']  != "" && $settings['settingsgeneral_vcard_long']  != "" ) : ?>
			<div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
				<meta itemprop="latitude" content="<?php echo $settings['settingsgeneral_vcard_lat'] ; ?>"/>
				<meta itemprop="longitude" content="<?php echo $settings['settingsgeneral_vcard_long'] ; ?>"/>
			</div>
			<?php endif; ?>
		</div><!-- .vcard -->

		<?php 
	}

	/* 		Helpers */
	
	/**
	 * Return a telephone number normalized for use within a href=tel:
	 */
	public function normalize_tel_no( $tel ) {

		$tel = preg_replace( '/\s|\-|\(0\)+/' ,'' ,$tel );
		$tel = preg_replace( '/^0/', '0044', $tel);
		$tel = preg_replace( '/\+/', '00', $tel);
		$tel = preg_replace( '/^([^0])/', "00$1", $tel);
		
		return $tel;
	}

	/*
	 * Return string with each line wrapped in an element
	 */
	public function tag_string( $adr, $tag = "b" ) {

		$arr = explode( "\n", $adr );

		if ( !is_array($arr)) return $adr;


		$output = "";

		foreach( $arr as $row ) {
			$output .= "<$tag>$row</$tag>";
		} 

		return $output;
	}
}

// Initalize the your plugin
$BCMH_Dev = new BCMH_Dev();

// Add an activation hook
register_activation_hook( __FILE__, array( &$BCMH_Dev, 'activate' ) );

// Run the plugins initialization method
add_action( 'init', array( &$BCMH_Dev, 'initialize' ) );

?>