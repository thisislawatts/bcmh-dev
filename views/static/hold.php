<!DOCTYPE html>
<!--[if lt IE 7]> <html dir="ltr" lang="en-US" id="h" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html dir="ltr" lang="en-US" id="h" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html dir="ltr" lang="en-US" id="h" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html ldir="ltr" lang="en-US" class="no-js"> <!--<![endif]-->
<head>
<!--

       BBBBBBBBBBBBBBBBBBB  CCCCCCCCCCCCCCCCCCC        MMMMMMMMMM             HHHHHHHHHM     
       B                 B  C                 C      MM          MM         HH          HH   
       B                 B  C                 C    :M              M      :H              H  
       B     BBBBBBB,    B  C      CCCCC,     C   ~M  M          M  M    ~H    H      H    H 
       B     BB    BB    B  C    CC     CC    C   M   MM        MM   M   H     H      H     H
       B     BB    BB    B  C   CC            C   M   M M      M M   M   H     H      H     H
       B     BBBBBB+     B  C   CC            C   M   M  M    M  M   M   H     HHHHHHHH     H
       B     BB    BB    B  C   CC            C   M   M  M    M  M   M   H     H      H     H
       B     BB    BB    B  C    CC     CC,   C   M   M   M  M   M   M   H     H      H    +H
       B     BBBBBBB     B  C      CCCCC      C    M  M    MM    M  M     H    H      H    H 
       B                 B  C                 C     M,             M       H             HH  
       B                 B  C                 C       MM,        MM         HH          HH    
       BBBBBBBBBBBBBBBBBBB  CCCCCCCCCCCCCCCCCCC         MMMMMMMMM             HHHHHHHHHH      

	   Bravo Charlie Mike Hotel
	   http://bcmh.co.uk

-->

		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width" />

		<title><?= bloginfo(); ?></title>

              <link rel="stylesheet" href="<?= $this->plugin_url; ?>css/login.css">

	</head>
	<body class="hold">
		<a id="BCMH_login" href="<?php echo wp_login_url( home_url() ); ?>">
                     <h1>BCMH</h1>
              </a>
	</body>
       <?php wp_footer(); ?>
</html>