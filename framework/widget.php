<?php

class BCMHWidget extends WP_Widget {
	function BCMHWidget() {
		parent::__construct( false, 'vCard Widget');
	}

	function widget( $args, $instance ) {
		// Widget Output
		do_shortcode( '[BCMHvcard]' );
	}

	function update( $new_instance, $old_instance ) {
		// Save widget options
	}

	function form( $instance ) {
		// Output admin widget options form
	}
}
